const keys = require('../keys');

module.exports = function(email, token) {
  return {
    to: email,
    from: keys.EMAIL_FROM,
    subject: 'Восстановление пароля',
    html: `
      <p>Вы запросили сброс пароля?</p>
      <p>Если нет, то проигнорурйте данное письмо</p>
      <p>Если да, то перейдите по ссылке</p>
      <p><a href="${keys.BASE_URL}/auth/password/${token}">Восстановить доступ</a></p>
      <hr/>
     <a href="${keys.BASE_URL}">Библиотекарь</a>
    `
  }
}
