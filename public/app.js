const toCurrency = price => {
  return new Intl.NumberFormat('ru-RU', {
    currency: 'rub',
    style: 'currency',
  }).format(price);
};

const toDate = date => {
  return new Intl.DateTimeFormat('ru-RU', {
    day:'2-digit',
    month: 'long',
    year: 'numeric',
    hour: '2-digit',
    minute: '2-digit',
    second: '2-digit',
  }).format(new Date(date));

};

document.querySelectorAll('.price').forEach(node => {
  node.textContent = toCurrency(node.textContent);
});

document.querySelectorAll('.date').forEach(node => {
  node.textContent = toDate(node.textContent);
});

const $cart = document.querySelector('#cart');

if ($cart) {
  $cart.addEventListener('click', event => {
    if (event.target.classList.contains('js-remove')) {
      const id = event.target.dataset.id;
      const csrf = event.target.dataset.csrf;
      fetch('cart/remove/' + id, {
        method: 'delete',
        headers: {
          'X-XSRF-TOKEN': csrf
        },
      }).then(res => res.json()).then(cart => {
        if (cart.books.length) {
          const html = cart.books.map(book => {
            return `
            <tr>
                <td>${book.title}</td>
                <td>${book.count}</td>
                <td class="price">${book.price * book.count}</td>
                <td>
                    <button class="btn btn-small js-remove" data-id="${book.id}">Удалить</button>
                </td>
            </tr>
            `;
          }).join('');

          $cart.querySelector('tbody').innerHTML = html;
          $cart.querySelector('span.price').innerHTML = toCurrency(cart.price);
        } else {
          $cart.innerHTML = '<p>Корзина пуста</p>';
        }
      });
    }
  });
}

M.Tabs.init(document.querySelector('.tabs'), {});


