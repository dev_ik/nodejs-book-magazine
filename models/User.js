const {Schema, model} = require('mongoose');

const userSchema = new Schema({
  email: {
    type: String,
    required: true
  },
  name: String,
  password: {
    type: String,
    required: true
  },
  cart: {
     items: [
       {
         count: {
           type: Number,
           required: true,
           default: 1
         },
         bookId: {
           type: Schema.Types.ObjectId,
           ref: 'Book',
           required: true
         }
       }
     ]
  },
  resetToken: String,
  resetTokenExt: Date,
  avatarUrl: String
})

userSchema.methods.addToCart = function(book) {
  const items = [...this.cart.items];
  const idx = items.findIndex(item => item.bookId.toString() === book._id.toString());

  idx >= 0
      ? items[idx].count++
      : items.push({
        bookId: book._id,
        count: 1
      })

  this.cart = {items};

  return this.save();
}

userSchema.methods.removeFromCart = function(bookId) {
  let items = [...this.cart.items];
  const idx = items.findIndex(item => {
    return item.bookId.toString() === bookId.toString()
  });

  if (items[idx].count === 1) {
    items = items.filter(item => item.bookId.toString() !== bookId.toString());
  } else {
    items[idx].count--;
  }

  this.cart = {items}


  return this.save();
}

userSchema.methods.clearCart = function() {
  this.cart = {items: []};
  return this.save();
}

module.exports = model('User', userSchema)
