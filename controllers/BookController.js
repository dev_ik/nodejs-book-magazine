const Book = require('../models/Book');
const {isOwner} = require('../utils/helpers');
const {validationResult} = require('express-validator');

module.exports = {
  async indexAction(req, res) {
    try {
      const books = await Book.find().lean();
      res.render('books/books', {
        title: 'Книги',
        isBooks: true,
        userId: req.user ? req.user._id.toString() : null,
        books,
      });
    } catch (e) {
      console.log(e);
    }
  },

  async showAction(req, res) {
    try {
      const book = await Book.findById(req.params.id).lean();
      res.render('books/book', {
        layout: 'empty',
        title: `Книга: "${book.title}"`,
        book,
      });
    } catch (e) {
      console.log(e);
    }
  },

  createPageAction(req, res) {
    res.render('add', {
      title: 'Добавить книгу',
      isAdd: true,
    });
  },

  async createAction(req, res) {
    const errors = validationResult(req);
    const body = req.body;

    if (!errors.isEmpty()) {
      return res.status(422).render('add', {
        title: 'Добавить книгу',
        isAdd: true,
        error: errors.array()[0].msg,
        data: {...body},
      });
    }

    const book = new Book({
      ...body,
      userId: req.user,
    });

    try {
      await book.save();
      res.redirect('/books');
    } catch (e) {
      console.log(e);
    }
  },

  async editAction(req, res) {
    try {
      switch (req.method) {
        case 'GET':
          if (!req.query.allow) {
            return res.redirect('/');
          }
          const bookGET = await Book.findById(req.params.id).lean();

          if (!isOwner(bookGET, req)) {
            return res.redirect('/books');
          }

          return res.render('books/book-edit', {
            title: `Редактирование книги: "${bookGET.title}"`,
            book: bookGET,
          });
        case 'POST':
          const errors = validationResult(req);
          const {id} = req.body;
          delete req.body.id;

          if (!errors.isEmpty()) {
            return res.status(422).redirect(`/books/${id}/edit ?allow=true`);
          }
          const bookPOST = await Book.findById(id);
          if (!isOwner(bookPOST, req)) {
            return res.redirect('/books');
          }

          Object.assign(bookPOST, req.body);
          await bookPOST.save();

          return res.redirect('/books');
      }
    } catch (e) {
      console.log(e);
    }
  },

  async removeAction(req, res) {
    try {
      await Book.deleteOne({
        _id: req.body.id,
        userId: req.user._id,
      });
      res.redirect('/books');
    } catch (e) {
      console.log(e);
    }
  },
};
