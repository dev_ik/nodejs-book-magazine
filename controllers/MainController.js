module.exports = {
  indexAction(req, res) {
    res.render('index', {
      title: 'Главная страница',
      isHome: true,
    });
  },
};
