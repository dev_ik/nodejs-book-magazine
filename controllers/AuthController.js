const {validationResult} = require('express-validator');
const User = require('../models/User');

module.exports = {
  async indexAction(req, res) {
    res.render('auth/login', {
      title: 'Авторизация',
      isLogin: true,
      registerError: req.flash('registerError'),
      loginError: req.flash('loginError'),
    });
  },

  resetAction(req, res) {
    res.render('auth/reset', {
      title: 'Восстановление пароля',
      error: req.flash('error'),
    });
  },

  async logoutAction(req, res) {
    req.session.destroy(() => {
      res.redirect('/auth/login#login');
    });
  },

  async authAction (req, res) {
  try {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      req.flash('loginError', errors.array()[0].msg);
      return res.status(422).redirect('/auth/login#login');
    }

    const {email} = req.body;

    req.session.user = await User.findOne({email: email});
    req.session.isAuthenticated = true;

    req.session.save(err => {
      if (err) {
        throw err;
      }
      res.redirect('/');
    });
  } catch (e) {
    console.log(e);
  }
}
};
