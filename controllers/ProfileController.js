const User = require('../models/User');
const {validationResult} = require('express-validator');

module.exports = {
  indexAction(req, res) {
    res.render('profile', {
      title: 'Профиль',
      isProfile: true,
      user: req.user.toObject(),
      error: req.flash('profileError'),
    });
  },

  async editAction(req, res) {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      req.flash('profileError', errors.array()[0].msg);
      return res.status(422).redirect('/profile');
    }

    try {
      const user = await User.findById(req.user._id);

      const toChange = {name: req.body.name};

      if (req.file) {
        toChange.avatarUrl = req.file.path.replace(/\\/ig, '/');
      }

      Object.assign(user, toChange);
      await user.save();
      res.redirect('/profile');
    } catch (e) {
      console.log(e);
    }
  },
};
