const Book = require('../models/Book');
const {computePrice, mapCartItems} = require('../utils/helpers');

module.exports = {
  async indexAction(req, res) {
    const user = await req.user.populate('cart.items.bookId').execPopulate();

    const books = mapCartItems(user.cart);
    res.render('cart', {
      title: 'Корзина',
      books,
      price: computePrice(books),
      isCart: true,
    });
  },

  async addAction(req, res) {
    const book = await Book.findById(req.body.id);
    await req.user.addToCart(book);
    res.redirect('/cart');
  },

  async removeAction(req, res) {
    await req.user.removeFromCart(req.params.id);
    const user = await req.user.populate('cart.items.bookId').execPopulate();
    const books = mapCartItems(user.cart);

    res.json({
      books,
      price: computePrice(books),
    });
  }
};
