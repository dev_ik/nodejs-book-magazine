const {validationResult} = require('express-validator');
const User = require('../models/User');
const bcrypt = require('bcryptjs');
const registerMail = require('../mails/registration');
const {transporter} = require('../utils/helpers');

module.exports = {
  async registerAction (req, res) {
  try {
    const {email, password, name} = req.body;

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      req.flash('registerError', errors.array()[0].msg);
      return res.status(422).redirect('/auth/login#register');
    }

    const hashPassword = await bcrypt.hash(password, 10);

    const user = new User({
      email, name, password: hashPassword, cart: {items: []},
    });

    await user.save();

    res.redirect('/auth/login#login');

    await transporter.sendMail(registerMail(email));

  } catch (e) {
    console.log(e);
  }
}
}
