const Order = require('../models/Order');
const {computePrice} = require('../utils/helpers');

module.exports = {
  async indexAction(req, res) {
    try {
      const orders = await Order.find({'user.userId': req.user._id}).
      populate('user.userId');

      res.render('order-history', {
        isOrder: true,
        title: 'Заказы',
        orders: orders.map(order => {
          return {
            ...order._doc,
            price: computePrice(order.books),
          };
        }),
        runtimeOptions: {
          allowProtoMethodsByDefault: true,
          allowProtoPropertiesByDefault: true,
        },
      });
    } catch (e) {
      console.log(e);
    }
  },

  async createAction(req, res) {

  try {
    const user = await req.user.populate('cart.items.bookId').
    execPopulate();

    const books = user.cart.items.map(item => ({
      count: item.count,
      book: {...item.bookId._doc, price:item.bookId._doc.price},
    }));

    const order = new Order({
      user: {
        name: req.user.name,
        userId: req.user,
      },
      books,
      price: computePrice(books)
    });

    await order.save();

    await req.user.clearCart();

    res.redirect('/order/history');
  } catch (e) {
    console.log(e);
  }
}
};
