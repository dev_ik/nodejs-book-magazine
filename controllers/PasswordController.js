const User = require('../models/User');
const bcrypt = require('bcryptjs');
const crypto = require('crypto');
const resetMail = require('../mails/reset');
const {transporter} = require('../utils/helpers');

module.exports = {
  async indexAction(req, res) {
    try {
      if (!req.params.token) {
        return res.redirect('/auth/login#login');
      }

      const user = await User.findOne({
        resetToken: req.params.token,
        resetTokenExt: {$gt: Date.now()},
      });

      if (!user) {
        return res.redirect('/auth/login#login');
      }

      res.render('auth/password', {
        title: 'Восстановление пароля',
        error: req.flash('error'),
        userId: user._id.toString(),
        token: req.params.token,
      });
    } catch (e) {
      console.log(e);
    }
  },

  async changeAction(req, res) {
    try {
      const user = await User.findOne({
        _id: req.body.userId,
        resetToken: req.body.token,
        resetTokenExt: {$gt: Date.now()},
      });
      if (!user) {
        req.flash('loginError',
            'Ссылка устарела, для смены пароля, запросите новую ссылку');
        return res.redirect('/auth/login#login');
      }

      user.password = await bcrypt.hash(req.body.password, 10);
      user.resetToken = undefined;
      user.resetTokenExt = undefined;

      await user.save();

      req.flash('loginError', 'Пароль изменён. Вы можете войти');
      res.redirect('/auth/login#login');

    } catch (e) {
      console.log(e);
    }
  },

  resetAction(req, res) {
    try {
      crypto.randomBytes(32, async (err, buffer) => {
        if (err) {
          req.flash('error', 'Что-то пошло не так, попробуйте позже');
          return res.redirect('/auth/reset');
        }

        const candidate = await User.findOne({email: req.body.email});

        if (!candidate) {
          req.flash('error', 'Пользователя не существует');
          return res.redirect('/auth/reset');
        }

        const token = buffer.toString('hex');
        candidate.resetToken = token;
        candidate.resetTokenExt = Date.now() + 60 * 60 * 1000;

        await candidate.save();

        res.redirect('/auth/login#login');

        await transporter.sendMail(resetMail(candidate.email, token));

      });
    } catch (e) {
      console.log(e);
    }
  },
};
