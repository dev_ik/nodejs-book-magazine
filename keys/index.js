const {parsed} = require('dotenv').config();

module.exports = {
  MONGODB_URI: parsed.MONGODB_URI,
  SESSION_SECRET_KEY: parsed.SESSION_SECRET_KEY,
  SEND_GRID_API_KEY: parsed.SEND_GRID_API_KEY,
  EMAIL_FROM: parsed.EMAIL_FROM,
  BASE_URL: parsed.BASE_URL + ':' + parsed.PORT,
  PORT: parsed.PORT
}
