const nodemailer = require('nodemailer');
const sendgrid = require('nodemailer-sendgrid-transport');
const keys = require('../keys');
const transporter = nodemailer.createTransport(sendgrid({
  auth: {api_key: keys.SEND_GRID_API_KEY},
}));

module.exports = {
  computePrice(books) {
    return books.reduce((total, book) => {
      let price = book.price || book.book.price;
      return total += price * book.count;
    }, 0);
  },

  mapCartItems(cart) {
    return cart.items.map(item => ({
      id: item.bookId._id,
      ...item.bookId._doc,
      count: item.count,
    }));
  },

  isOwner(book, req) {
    return book.userId.toString() === req.user._id.toString();
  },

  transporter
};
