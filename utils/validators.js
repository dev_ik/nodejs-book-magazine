const {body} = require('express-validator');
const User = require('../models/User');
const bcrypt = require('bcryptjs');

module.exports.registerValidators = [
  body('email').
  isEmail().
  withMessage('Указан некорректный email').
  custom(async (value, {req}) => {
    try {
      const user = await User.findOne({email: value});
      if (user) {
        return Promise.reject('Такой email уже занят');
      }
    } catch (e) {
      console.log(e);
    }
  }).
  normalizeEmail(),

  body('password').
  isLength({min: 3, max: 6}).
  withMessage('Пароль должен быть минимум 6 символов').
  isAlphanumeric().
  trim(),

  body('repeat').custom((value, {req}) => {
    if (value !== req.body.password) {
      throw new Error('Пароли должны совпадать');
    }
    return true;
  }).trim(),

  body('name').
  isLength({min: 3}).
  withMessage('Имя должно быть минимум 3 символа').
  trim(),
];

module.exports.loginValidators = [
  body('email').
  isEmail().
  withMessage('Указан некорректный email').
  custom(async (value, {req}) => {
    try {
      const user = await User.findOne({email: value});
      if (!user) {
        return Promise.reject('Неверный логин или пароль');
      }
    } catch (e) {
      console.log(e);
    }
  }).
  normalizeEmail(),

  body('password', 'Неверно указан пароль').
  isLength({min: 3, max: 6}).
  custom(async (value, {req}) => {
    try {
      const user = await User.findOne({email: req.body.email});
      if (!user) {
        return Promise.reject('Неверный логин или пароль');
      }
      const validPassword = await bcrypt.compare(value, user.password);
      if (!validPassword) {
        return Promise.reject('Неверный логин или пароль');
      }
    } catch (e) {
      console.log(e);
    }

  }).
  trim(),
];

module.exports.bookValidators = [
  body('title', 'Минимальная длина названия 3 символа').
  isLength({min: 3}).
  trim(),

  body('price', 'Введите корректную цену').isNumeric(),

  body('img', 'введите корректный url картинки').isURL(),
];

module.exports.profileValidators = [
  body('name').
  isLength({min: 3}).
  withMessage('Имя должно быть минимум 3 символа').
  trim(),
];
