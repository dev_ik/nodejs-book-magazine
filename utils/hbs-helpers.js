module.exports = {
  multiply: (a, b) => a * b,
  compare(a, b, options) {
    return a.toString() === b.toString()
        ? options.fn(this)
        : options.inverse(this);
  }
};
