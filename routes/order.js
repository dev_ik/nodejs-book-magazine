const {Router} = require('express');
const router = new Router();
const auth = require('../middleware/authenticate');
const OrderController = require('../controllers/OrderController')

router.get('/history', auth, OrderController.indexAction);

router.post('/create', auth, OrderController.createAction);

module.exports = router;
