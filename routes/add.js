const {Router} = require('express');
const router = new Router();
const auth = require('../middleware/authenticate');
const {bookValidators} = require('../utils/validators');
const BookController = require('../controllers/BookController');

router.get('/', auth, BookController.createPageAction);

router.post('/', auth, bookValidators, BookController.createAction);

module.exports = router;
