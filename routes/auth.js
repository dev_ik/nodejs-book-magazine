const {Router} = require('express');
const router = new Router();
const {registerValidators, loginValidators} = require('../utils/validators');
const AuthController = require('../controllers/AuthController');
const PasswordController = require('../controllers/PasswordController');
const RegisterController = require('../controllers/RegisterController');

router.get('/login', AuthController.indexAction);

router.get('/reset', AuthController.resetAction);

router.get('/logout', AuthController.logoutAction);

router.get('/password/:token', PasswordController.indexAction);

router.post('/password', PasswordController.changeAction);

router.post('/reset', PasswordController.resetAction);

router.post('/login', loginValidators, AuthController.authAction);

router.post('/register', registerValidators, RegisterController.registerAction);

module.exports = router;
