const {Router}  = require('express');
const router = new Router();
const auth = require('../middleware/authenticate');
const CartController = require('../controllers/CartController')

router.get('/', auth, CartController.indexAction);

router.post('/add', auth, CartController.addAction);

router.delete('/remove/:id', auth, CartController.removeAction);

module.exports = router;
