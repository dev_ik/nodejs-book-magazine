const {Router} = require('express');
const router = new Router();
const auth = require('../middleware/authenticate');
const {profileValidators} = require('../utils/validators');
const ProfileController = require('../controllers/ProfileController');

router.get('/', auth, ProfileController.indexAction);

router.post('/', auth, profileValidators, ProfileController.editAction);

module.exports = router;
