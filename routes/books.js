const {Router} = require('express');
const router = new Router();
const auth = require('../middleware/authenticate');
const {bookValidators} = require('../utils/validators');
const BookController = require('../controllers/BookController');

router.get('/', BookController.indexAction);

router.get('/:id', BookController.showAction);

router.get('/:id/edit', auth, BookController.editAction);

router.post('/remove', auth, BookController.removeAction);

router.post('/edit', auth, bookValidators, BookController.editAction);

module.exports = router;
